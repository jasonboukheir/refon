﻿using UnityEngine;

namespace RefOn.Tests.Props
{
    public class TestMonoBehaviour : MonoBehaviour
    {
        [RefOn]
        public GameObject publicRequiredReference;

        [RefOn]
        [SerializeField]
        private GameObject privateRequiredReference;
    }
}
