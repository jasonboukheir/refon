﻿using UnityEngine;

namespace RefOn.Tests.Props
{
    [CreateAssetMenu(
        menuName = "RefOn/TestScriptableObject",
        fileName = "NewTestScriptableObject"
    )]
    public class TestScriptableObject : ScriptableObject
    {
        [RefOn]
        public TestScriptableObject publicRequiredReference;

        [RefOn]
        [SerializeField]
        private TestScriptableObject privateRequiredReference;
    }
}
