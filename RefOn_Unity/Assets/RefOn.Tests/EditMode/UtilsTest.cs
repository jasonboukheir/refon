using System.Collections;
using NUnit.Framework;

namespace RefOn.Tests
{
    public class UtilsUnitTest
    {
    }

    public class UtilsUnitTestCases
    {
        public static IEnumerable TestRequiredFieldsCases
        {
            get
            {
                yield return new TestCaseData(new RequiredFieldsTestClass(null)).Returns(1);
                yield return new TestCaseData(new RequiredFieldsTestClass("")).Returns(0);
            }
        }

    }

    public class RequiredFieldsTestClass
    {
        [RefOn]
        public string requiredString;

        [RefOn]
        private int requiredInt;

        public RequiredFieldsTestClass(string requiredString)
        {
            this.requiredString = requiredString;
        }
    }
}
