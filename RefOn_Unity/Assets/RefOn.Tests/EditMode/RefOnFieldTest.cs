using System.Reflection;
using NUnit.Framework;
using RefOn.Editor.Assets;
using UnityEngine;

namespace RefOn.Tests.EditMode
{
    public class RefOnFieldTest
    {
        private const BindingFlags INSTANCE_BINDING_FLAGS =
            BindingFlags.NonPublic |
            BindingFlags.Public |
            BindingFlags.Instance;

        private TestComponent component;

        private RefOnField refOnField;

        [OneTimeSetUp]
        public void SetUpComponent()
        {
            component = new GameObject().AddComponent<TestComponent>();
        }

        [OneTimeTearDown]
        public void TearDownGameObject()
        {
            GameObject.DestroyImmediate(component.gameObject);
        }

        [SetUp]
        public void SetUpRefOnField()
        {
            refOnField = new RefOnField(component, GetNameField());
        }

        [Test]
        public void ValueReturnsFieldValue(
            [Values("name", "", null)] string expected)
        {
            component.stringField = expected;
            Assert.AreEqual(refOnField.Value as string, expected);
        }

        private FieldInfo GetNameField()
        {
            return component.GetType().GetField("stringField", INSTANCE_BINDING_FLAGS);
        }
    }

    public class TestComponent : MonoBehaviour
    {
        public string stringField;
    }
}
