using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace RefOn.Editor.Assets
{
    public static class UnityObjectExtensions
    {
        private const BindingFlags INSTANCE_BINDING_FLAGS =
            BindingFlags.NonPublic |
            BindingFlags.Public |
            BindingFlags.Instance;

        public static IEnumerable<RefOnField> GetRefOnFields(this UnityEngine.Object instance)
        {
            return from field in GetRefOnMarkedFields(instance.GetType())
                   select new RefOnField(instance, field);
        }

        private static IEnumerable<FieldInfo> GetRefOnMarkedFields(Type type)
        {
            return from field in type.GetFields(INSTANCE_BINDING_FLAGS)
                   where field.IsDefined(typeof(RefOnAttribute), true)
                   select field;
        }
    }

}
