using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace RefOn.Editor.Assets
{
    public class Referee
    {
        private const BindingFlags INSTANCE_BINDING_FLAGS =
            BindingFlags.NonPublic |
            BindingFlags.Public |
            BindingFlags.Instance;

        private readonly UnityEngine.Object componentOrScriptableObject;

        public Referee(UnityEngine.Object componentOrScriptableObject)
        {
            ValidateUnityObject();
            this.componentOrScriptableObject = componentOrScriptableObject;
        }

        public static implicit operator Referee(UnityEngine.Object instance)
            => new Referee(instance);

        public IEnumerable<RefOnField> GetRefOnFields()
        {
            return from field in GetRefOnFieldInfos()
                   select new RefOnField(componentOrScriptableObject, field);
        }

        private IEnumerable<FieldInfo> GetRefOnFieldInfos()
        {
            return from field in GetFieldInfos()
                   where FieldHasRefOnAttribute(field)
                   select field;
        }

        private IEnumerable<FieldInfo> GetFieldInfos()
        {
            return componentOrScriptableObject.GetType()
                .GetFields(INSTANCE_BINDING_FLAGS);
        }

        private bool FieldHasRefOnAttribute(FieldInfo field)
        {
            return field.IsDefined(typeof(RefOnAttribute), true);
        }

        private void ValidateUnityObject()
        {
            switch (componentOrScriptableObject)
            {
                case MonoBehaviour m:
                case ScriptableObject s:
                    break;
                default:
                    throw new NotSupportedException("unityObject must be a MonoBehaviour or ScriptableObject.");
            }
        }
    }
}
