﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RefOn.Editor.Assets
{
    public class RefOnAsset
    {
        private readonly UnityEngine.Object asset;

        public RefOnAsset(UnityEngine.Object asset)
        {
            this.asset = asset;
            ValidateAsset();
        }

        public static implicit operator RefOnAsset(UnityEngine.Object asset)
            => new RefOnAsset(asset);

        public IEnumerable<RefOnField> GetRefOnFields()
        {
            return GetReferees().SelectMany(referee => referee.GetRefOnFields());
        }

        private IEnumerable<Referee> GetReferees()
        {
            switch (asset)
            {
                case SceneAsset sceneAsset:
                    return GetReferees(sceneAsset);
                case GameObject gameObject:
                    return GetReferees(gameObject);
                case ScriptableObject scriptableObject:
                    return GetReferees(scriptableObject);
                default:
                    return null;
            }
        }

        private IEnumerable<Referee> GetReferees(SceneAsset sceneAsset)
        {
            return GetReferees(sceneAsset.OpenScene());
        }

        private IEnumerable<Referee> GetReferees(GameObject gameObject)
        {
            return from component in gameObject.GetComponentsInChildren<MonoBehaviour>(true)
                   select new Referee(component);
        }

        private IEnumerable<Referee> GetReferees(ScriptableObject scriptableObject)
        {
            return new Referee[1] { scriptableObject };
        }

        private IEnumerable<Referee> GetReferees(Scene scene)
        {
            return scene.GetRootGameObjects().SelectMany(gameObject => GetReferees(gameObject));
        }

        private void ValidateAsset()
        {
            switch (asset)
            {
                case SceneAsset scene:
                case GameObject prefab:
                case ScriptableObject scriptableObject:
                    break;
                default:
                    throw new NotSupportedException("asset must be a SceneAsset, GameObject, or ScriptableObject.");
            }
        }
    }
}
