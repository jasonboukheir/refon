﻿using System.Reflection;

namespace RefOn.Editor.Assets
{
    public class RefOnField
    {
        public readonly FieldInfo field;

        public readonly UnityEngine.Object instance;

        public RefOnField(UnityEngine.Object instance, FieldInfo field)
        {
            this.field = field;
            this.instance = instance;
        }

        public bool IsNull =>
            Value == null;

        public object Value =>
            field.GetValue(instance);
    }
}
