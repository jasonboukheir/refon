﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static UnityEditor.AssetDatabase;

namespace RefOn.Editor.Assets
{
    public class AssetFindingUtility
    {
        private static readonly IEnumerable<Type> SUPPORTED_ASSET_TYPES = new Type[3]
        {
            typeof(SceneAsset),
            typeof(GameObject),
            typeof(ScriptableObject)
        };

        public static IEnumerable<RefOnField> GetAllRefOnFields()
        {
            return SUPPORTED_ASSET_TYPES.
                SelectMany(assetType => GetAll(assetType)).
                Select(asset => new RefOnAsset(asset)).
                SelectMany(refOnAsset => refOnAsset.GetRefOnFields());
        }

        private static IEnumerable<UnityEngine.Object> GetAll(Type type)
        {
            return from guid in GetAllGuidsFor(type)
                   select LoadAssetFromGuid(guid);
        }

        private static string[] GetAllGuidsFor(Type type)
        {
            return FindAssets($"t:{type.Name}");
        }

        private static UnityEngine.Object LoadAssetFromGuid(string guid)
        {
            return LoadAssetAtPath<UnityEngine.Object>(
                GUIDToAssetPath(guid));
        }
    }
}
