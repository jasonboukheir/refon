using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace RefOn.Editor.Assets
{
    public static class SceneAssetExtensions
    {
        public static Scene OpenScene(this SceneAsset sceneAsset)
        {
            var path = AssetDatabase.GetAssetPath(sceneAsset);
            return EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
        }

    }
}
