﻿using System.Collections.Generic;
using NUnit.Framework;
using RefOn.Editor.Assets;
using UnityEngine;
using static RefOn.Editor.Assets.AssetFindingUtility;

namespace RefOn.Editor.Validation
{
    public class NullReferenceTest
    {
        [Test]
        public void ValidateRefOnFields()
        {
            ValidateRefOnFields(GetAllRefOnFields());
        }

        private void ValidateRefOnFields(IEnumerable<RefOnField> refOnFields)
        {
            foreach (var refOnField in refOnFields)
                if (refOnField.IsNull)
                    ReportNullRefOnField(refOnField);
        }

        private void ReportNullRefOnField(RefOnField refOnField)
        {
            Debug.LogError(
                $"Found null reference on {refOnField.instance.name}.",
                refOnField.instance);
        }
    }
}
