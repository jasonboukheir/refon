﻿using System;

namespace RefOn
{
    [AttributeUsage(AttributeTargets.Field)]
    public class RefOnAttribute : Attribute { }
}
